
public class Car {
	private String color;
	private String model;
	private int maxSpeed;
	private int standardPrice;
	private double drivenKilometers;
	private double literPer100km;
	private Producer producer;
	private Engine engine;

	public Car(String color, String model, int maxSpeed, int standardPrice, double drivenKilometers, double literPer100km, Producer producer, Engine engine) {
		this.color = color;
		this.model = model;
		this.maxSpeed = maxSpeed;
		this.standardPrice = standardPrice;
		this.literPer100km = literPer100km;
		this.drivenKilometers = drivenKilometers;
		this.producer = producer;
		this.engine = engine;
	}
	
	public String getColor() {
		return color;
	}
	public void setColor(String color) {
		this.color = color;
	}
	public String getModel() {
		return model;
	}
	public void setModel(String model) {
		this.model = model;
	}
	public Engine getEngine() {
		return engine;
	}
	public void setEngine(Engine engine) {
		this.engine = engine;
	}
	public int getMaxSpeed() {
		return maxSpeed;
	}
	public void setMaxSpeed(int maxSpeed) {
		this.maxSpeed = maxSpeed;
	}
	public int getStandardPrice() {
		return standardPrice;
	}
	public void setStandardPrice(int standardPrice) {
		this.standardPrice = standardPrice;
	}
	public double getLiterPer100km() {
		return literPer100km;
	}
	public void setLiterPer100km(double literPer100km) {
		this.literPer100km = literPer100km;
	}
	public Producer getProducer() {
		return producer;
	}
	public void setProducer(Producer producer) {
		this.producer = producer;
	}
	public double getDrivenKilometers() {
		return drivenKilometers;
	}

	public void setDrivenKilometers(double drivenKilometers) {
		this.drivenKilometers = drivenKilometers;
	}

	
	public double getPrice() {
		double Price = this.getStandardPrice() * ((100 - this.getProducer().getDiscountInPercent())/100);
		return Price;
	}
	public String getType() {
		String Type = this.getEngine().getType();
		return Type;
	}
	public double getUsage() {
		double Usage = 0;
		if(this.getDrivenKilometers() >= 50000 && this.getEngine().getType() == "benzin") {
			Usage = this.getLiterPer100km() * 0.098 + this.getLiterPer100km();
		}
		else {
			Usage = this.getLiterPer100km();		
		}
		return Usage;
	}

}
