import java.time.LocalDate;
import java.time.Month;

public class Main {

	public static void main(String[] args ) {
		Engine e1 = new Engine(150,"diesel");
		Engine e2 = new Engine(250,"benzin");
		
		Producer p1 = new Producer("Herbert", "Deutschland", 10);
		Producer p2 = new Producer("Erwin", "Oesterreich", 8);
		
		Car c1 = new Car("red", "Merschedes", 256, 10000, 29054, 5.5, p1, e2);
		Car c2 = new Car("blue", "ToiToiToyota", 355, 29000, 68985, 6.8, p2, e2);
		Car c3 = new Car("blue", "ToiToiToyota", 355, 29000, 68985, 6.8, p2, e1);
		
		Person pers1 = new Person("Guntram", "Geisberger", LocalDate.of(1999, Month.SEPTEMBER, 19));
		Person pers2 = new Person("Juenter", "Gauch", LocalDate.of(1964, Month.MARCH, 27));
		
		pers1.addCar(c1);	//F�gt Guntram Geisberger den roten Merschedes hinzu
		pers1.addCar(c3);	//F�gt Guntram Geisberger den blauen ToiToiToyota hinzu
		
		System.out.println(c1.getPrice());	//Ausgabe des Preises abz�glich des Rabattes von 10%
		System.out.println(c1.getType());	//Ausgabe des Spritypes des Motors
		System.out.println(c1.getUsage());	//Ausgabe der Spritverbrauchs unter 50000km benzin
		System.out.println(c2.getUsage());	//Ausgabe der Spritverbrauchs �ber 50000km diesel
		System.out.println(c3.getUsage());	//Ausgabe der Spritverbrauchs �ber 50000km benzin
		pers1.printCars();					//Ausgabe der Farbe der beiden Autos von Guntram Geisberger
		System.out.println(pers1.getValueOfCars()); //Ausgabe des Wertes aller Autos die Guntram Geisberger geh�ren
		System.out.println(pers1.getAge());	//Ausgabe des Alters von Guntram Geisberger
	}
}
