import java.time.LocalDate;
import java.time.Period;
import java.util.ArrayList;
import java.util.List;

public class Person {

	private String firstName, lastName;
	private LocalDate birthday;
	private List<Car> cars;

	public Person(String firstName, String lastName, LocalDate birthday) {
		super();
		this.firstName = firstName;
		this.lastName = lastName;
		this.cars = new ArrayList<>();
		this.birthday = birthday;
	}
	

	public void addCar(Car c) {
		this.cars.add(c);
	}
	
	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	
	public LocalDate getBirthday() {
		return birthday;
	}

	public void setBirthday(LocalDate birthday) {
		this.birthday = birthday;
	}

	public int getAge() {
		return Period.between(birthday,LocalDate.now()).getYears();
	}

	public void printCars() {
		for (Car car : cars) {
			System.out.println(car.getColor());
		}
	}
	
	public double getValueOfCars() {
		double value = 0;
		for (Car car : cars) {
			value = (car.getPrice()) + value;
		}
		return value;
	}
	
}
