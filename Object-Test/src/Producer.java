
public class Producer {

	private String name;
	private String country;
	private double discountInPercent;
	
	public Producer(String name, String country, double discountInPercent) {
		super();
		this.name = name;
		this.country = country;
		this.discountInPercent = discountInPercent;
	}

	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public double getDiscountInPercent() {
		return discountInPercent;
	}
	public void setDiscountInPercent(int discountInPercent) {
		this.discountInPercent = discountInPercent;
	}
	
}
